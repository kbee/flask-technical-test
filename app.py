import os
from dotenv import load_dotenv

load_dotenv()
from flask import Flask, jsonify, make_response, request
from functools import wraps
from db import db
from flask_migrate import Migrate
from flask_cors import CORS
import models


def create_app():
    app = Flask(__name__)
    app.config.from_object(os.getenv("APP_SETTINGS", "config.DevelopmentConfig"))
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    CORS(app)
    db.init_app(app)
    migrate = Migrate(app, db)

    return app


app = create_app()


# List all existing todos
@app.get("/todos/")
def get_todos():
    return {}


# List properties for a particular todo
@app.get("/todos/<string:id>")
def get_todo_infos(id):
    return {}


# Update a todo
@app.put("/todos/<string:id>")
def update_todo(id):
    update_infos = request.get_json()
    return


# Create a new todo
@app.post("/todos/")
def create_todo():
    create_infos = request.get_json()
    return {}


# Delete a todo
@app.delete("/todos/<string:id>/")
def delete_todo(id):
    return make_response(jsonify(message="Todo deleted!"), 200)


@app.errorhandler(404)
def resource_not_found(e):
    return make_response(jsonify(error="Not found!"), 404)
