from db import db


class TodoModel(db.Model):
    __tablename__ = "todos"

    id = db.Column(db.Integer, primary_key=True)

    def obj_to_dict(self):
        return {}
