## Flask technical test: A simple TODO management API

You want to create a todo management app, and you want to do an API to create, get, delete and update your todos.

Implement this features in this Flask app.

## Constraints
* Fill in the routes' functions in the app.py file
* Define your todo models
* Create a POSTGRESQL database and interact with it using SQLAlchemy

Bonus
* Manage migrations with flask-migrate
* Add a user managing system to assign todos to some users